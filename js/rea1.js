/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "RfANdzwiHkM",
    "titulo": "Soborno y Corrupción Parte 2 Evadir y Rechazar Sobornos sonido Video 03",
    "descripcion": "",
    "url": "RfANdzwiHkM",
    "preguntas": [
        {
            "second": "35",
            "question": "¿Sabes cuáles son los riesgos asociados al soborno?"
        }, {
            "second": "63",
            "question": "¿Conoces las maneras para rechazar un soborno?"
        }
    ]
}];    

